{
  "acronyms":[
    {
      "acronym": "AOS",
      "meaning": "Acquisition of Signal – a phrase used during launch when communication with the vehicle is re-established after Loss of Signal"
    },
    {
      "acronym": "ACS",
      "meaning": "Attitude Control System – synonymous with Reaction Control System"
    },
    {
      "acronym": "ACES",
      "meaning": "Advanced Cryogenic Evolved Stage - a LOX/LH2 upper stage being developed for ULA's Vulcan launch vehicle"
    },
    {
      "acronym": "ARM",
      "meaning": "Asteroid Redirect Mission - a planned NASA mission to redirect an asteroid to lunar orbit for rendezvous with an Orion spacecraft"
    },
    {
      "acronym": "ASDS",
      "meaning": "Autonomous Spaceport Drone Ship"
    },
    {
      "acronym": "ASOG",
      "meaning": "A Shortfall of Gravitas - moniker given to an upcoming Atlantic ASDS"
    },
    {
      "acronym": "BEAM",
      "meaning": "Bigelow Expandable Activities Module – an inflatable module designed by Bigelow Aerospace that will be brought up to the ISS aboard CRS-8"
    },
    {
      "acronym": "BFR",
      "meaning": "Big Falcon Rocket – a generic term for the next SpaceX rocket, which will be larger than Falcon Heavy"
    },
    {
      "acronym": "BO",
      "meaning": "Blue Origin – a competitor of SpaceX"
    },
    {
      "acronym": "C3",
      "meaning": "Characteristic Energy – a measure of escape velocity (< 1 = no escape, 1 = escape with no further velocity, > 1 = escape with increasing velocity)"
    },
    {
      "acronym": "CAA",
      "meaning": "Crew Access Arm - Arm that swings out from the FSS (Fixed Service Structure) to give crew access to Dragon while on the pad"
    },
    {
      "acronym": "CBM",
      "meaning": "Common Berthing Mechanism – a device used to attach the cargo Dragon to the ISS"
    },
    {
      "acronym": "CCAFS",
      "meaning": "Cape Canaveral Air Force Station"
    },
    {
      "acronym": "CCDev",
      "meaning": "Commercial Crew Development"
    },
    {
      "acronym": "CCiCap",
      "meaning": "Commercial Crew Integrated Capability"
    },
    {
      "acronym": "CCtCap",
      "meaning": "Commercial Crew Transportation Capability"
    },
    {
      "acronym": "COPV",
      "meaning": "Composite Overwrapped Pressure Vessel – a pressure tank constructed from a thin, non-structural liner wrapped with structural fiber composite"
    },
    {
      "acronym": "COTS",
      "meaning": "Commercial Orbital Transportation Services"
    },
    {
      "acronym": "CRS",
      "meaning": "Commercial Resupply Services – a contract that SpaceX and Orbital both have with NASA to resupply the International Space Station"
    },
    {
      "acronym": "Delta-v or Δv",
      "meaning": "Change in velocity – the amount of 'effort' required to move a spacecraft from one location or orbit to another"
    },
    {
      "acronym": "DPL",
      "meaning": "Downrange Propulsive Landing (on an ocean barge/ASDS)"
    },
    {
      "acronym": "EAR",
      "meaning": "Export Administration Regulations – a US law that regulates the export of technology with \"dual-use\" applications (both commercial and military)"
    },
    {
      "acronym": "EDL",
      "meaning": "Entry, Descent and Landing – the series of controlled manoeuvres and pathways used to land an orbiting spacecraft"
    },
    {
      "acronym": "FRR",
      "meaning": "Flight Readiness Review – a paperwork exercise used to ensure that a rocket is ready to fly"
    },
    {
      "acronym": "FSS",
      "meaning": "Fixed Service Structure – a structure on the pad at Launch Complex 39A, Cape Canaveral"
    },
    {
      "acronym": "F1",
      "meaning": "Falcon 1"
    },
    {
      "acronym": "F9",
      "meaning": "Falcon 9"
    },
    {
      "acronym": "FH",
      "meaning": "Falcon Heavy"
    },
    {
      "acronym": "FPIP",
      "meaning": "Flight Planning Integration Panel"
    },
    {
      "acronym": "FRR",
      "meaning": "Flight Readiness Review"
    },
    {
      "acronym": "FT",
      "meaning": "Full Thrust"
    },
    {
      "acronym": "GNC",
      "meaning": "Guidance, navigation and control (abbreviated GNC, GN&C, or G&C)"
    },
    {
      "acronym": "GSE",
      "meaning": "Ground Support Equipment – any ground-based equipment used to enable a launch to occur"
    },
    {
      "acronym": "HIF",
      "meaning": "Horizontal Integration Facility"
    },
    {
      "acronym": "IAC",
      "meaning": "International Astronautical Congress – an annual conference held to discuss matters of relevance to the aerospace industry"
    },
    {
      "acronym": "IIP",
      "meaning": "Instantaneous Impact Point – refers to the point a rocket would land at if all propulsion was terminated at that moment"
    },
    {
      "acronym": "IMMT",
      "meaning": "ISS Mission Management Team"
    },
    {
      "acronym": "ISRU",
      "meaning": "In-situ Resource Utilization – refers to the mining and processing of the regolith/atmosphere of Mars, Moon, etc, for use as fuel, propellant, or other consumables"
    },
    {
      "acronym": "Isp",
      "meaning": "Specific Impulse – a measure of the \"fuel efficiency\" of a rocket engine"
    },
    {
      "acronym": "ISS",
      "meaning": "International Space Station – an internationally run habitable artificial satellite, in low Earth orbit"
    },
    {
      "acronym": "ITAR",
      "meaning": "International Traffic in Arms Regulations – a US law that regulates the export and import of military technology (including rockets)"
    },
    {
      "acronym": "JRTI",
      "meaning": "Just Read The Instructions (Marmac 303) – a moniker given to SpaceX's Pacific ASDS."
    },
    {
      "acronym": "JWST",
      "meaning": "James Webb Space Telescope – a NASA space observatory under construction and scheduled to launch in 2018"
    },
    {
      "acronym": "KSC",
      "meaning": "Kennedy Space Center – a NASA-run orbital launch center at Cape Canaveral, Florida"
    },
    {
      "acronym": "L1",
      "meaning": "Lagrange Point 1 – usually used in reference to Earth-Sun L1, this is a region of space in which it is possible to \"hover\" in between the Earth and the Sun"
    },
    {
      "acronym": "L2",
      "meaning": "an exclusive members-only club of the NASA Spaceflight community, or Lagrange Point 2"
    },
    {
      "acronym": "LAS/LES",
      "meaning": "Launch Abort or Launch Escape System - the system that evacuates a capsule from a malfunctioning rocket"
    },
    {
      "acronym": "LC-13",
      "meaning": "Launch Complex 13, Cape Canaveral – SpaceX chosen landing pad for launches from SLC-40 & LC-39A"
    },
    {
      "acronym": "LC-39A",
      "meaning": "Launch Complex 39A, Cape Canaveral – a former shuttle launch site now leased by SpaceX"
    },
    {
      "acronym": "LOM",
      "meaning": "Loss of Mission – The mission failing irrecoverably, often due to the the launch vehicle failing/exploding"
    },
    {
      "acronym": "LOS",
      "meaning": "Loss of Signal – a phrase used during launch when communication with the vehicle is temporarily lost"
    },
    {
      "acronym": "LOV",
      "meaning": "Loss of Vehicle – Mission failure due to launch vehicle failure"
    },
    {
      "acronym": "LRR",
      "meaning": "Launch (vehicle) Readiness Review"
    },
    {
      "acronym": "LZ-1",
      "meaning": "SpaceX designation of LC-13, Launch Complex 13, Cape Canaveral – SpaceX chosen landing pad for launches from SLC-40 & LC-39A"
    },
    {
      "acronym": "MA",
      "meaning": "Mission Assurance – an engineering process used to increase the chance of mission success"
    },
    {
      "acronym": "MARS",
      "meaning": "Mid Atlantic Regional Spaceport – home of Orbital ATK's Minotaur and Antares rockets"
    },
    {
      "acronym": "MCT",
      "meaning": "Mars Colonial Transporter – a vehicle planned by SpaceX to transport humans to and from Mars"
    },
    {
      "acronym": "MECO",
      "meaning": "Main Engine Cutoff – during launch, the ending of the first stage engine burn"
    },
    {
      "acronym": "NDA",
      "meaning": "Non-Disclosure Agreement – a legal contract signed by employees, wherein they agree not to leak sensitive information outside their workplace"
    },
    {
      "acronym": "NDS",
      "meaning": "NASA Docking System – a device used to attach the crew Dragon to the ISS\nNET – No Earlier Than"
    },
    {
      "acronym": "NEO",
      "meaning": "Near Earth Orbit - usually used to denote an Earth-crossing asteroid"
    },
    {
      "acronym": "NSF",
      "meaning": "NASA Spaceflight/NASA Spaceflight Forum"
    },
    {
      "acronym": "OCISLY",
      "meaning": "Of Course I Still Love You (Marmac 304) – SpaceX's Atlantic ASDS barge"
    },
    {
      "acronym": "PIF",
      "meaning": "Payload Integration Facility"
    },
    {
      "acronym": "PICA-X",
      "meaning": "Phenolic Impregnated Carbon Ablator-X – material used for the heat shields of the Dragon capsules"
    },
    {
      "acronym": "PMA",
      "meaning": "Pressurized Mating Adapter – a device used on the ISS to convert a CBM port to an NDS port"
    },
    {
      "acronym": "PMF",
      "meaning": "Propellant Mass Fraction"
    },
    {
      "acronym": "RCS",
      "meaning": "Reaction Control System – synonymous with Attitude Control System"
    },
    {
      "acronym": "RSS",
      "meaning": "Rotating Service Structure – a structure on the pad at Launch Complex 39A, Cape Canaveral"
    },
    {
      "acronym": "RUD",
      "meaning": "Rapid Unplanned/Unscheduled Disassembly – rocket speak for catastrophic failure"
    },
    {
      "acronym": "RTLS",
      "meaning": "Return To Launch Site"
    },
    {
      "acronym": "SECO",
      "meaning": "Second (Stage) Engine Cutoff – during launch, the ending of the second stage engine burn"
    },
    {
      "acronym": "SEP",
      "meaning": "Solar Electric Propulsion"
    },
    {
      "acronym": "SLC-40",
      "meaning": "Space Launch Complex 40, Cape Canaveral"
    },
    {
      "acronym": "SLC-4E",
      "meaning": "Space Launch Complex 4 East, Vandenberg"
    },
    {
      "acronym": "SLC-4W",
      "meaning": "Space Launch Complex 4 West, Vandenberg – SpaceX's chosen landing location for launches from SLC-4E"
    },
    {
      "acronym": "SPIF",
      "meaning": "Spacecraft Processing and Integration Facility"
    },
    {
      "acronym": "SF",
      "meaning": "Static Fire – a short test prior to launch where the engines are fired for a few seconds"
    },
    {
      "acronym": "SFN",
      "meaning": "Spaceflight Now – a website specialising in spaceflight news"
    },
    {
      "acronym": "SSME",
      "meaning": "Space Shuttle Main Engine"
    },
    {
      "acronym": "SSTO",
      "meaning": "Single Stage To Orbit"
    },
    {
      "acronym": "T/E",
      "meaning": "Transporter/Erector – The structure that takes Falcon from a horizontal to vertical position, and holds the rocket in place on the pad"
    },
    {
      "acronym": "TEL",
      "meaning": "Transporter Erector Launcher – T/E equivalent, the structure that takes Falcon from a horizontal to vertical position, and holds the rocket in place on the pad"
    },
    {
      "acronym": "TRL",
      "meaning": "Technology Readiness Level – NASA's method of estimating technology maturity (9 = actively in use, 1 = very far from ready)"
    },
    {
      "acronym": "ULA",
      "meaning": "United Launch Alliance – a competitor of SpaceX"
    },
    {
      "acronym": "VAB",
      "meaning": "Vehicle Assembly Building"
    },
    {
      "acronym": "VAFB",
      "meaning": "Vandenberg Air Force Base, California – site of a SpaceX launch pad that is used for polar and retrograde-LEO flights"
    },
    {
      "acronym": "VV",
      "meaning": "Visiting Vehicle – a term for any vehicle that visits the ISS"
    },
    {
      "acronym": "WDR",
      "meaning": "Wet Dress Rehearsal – a procedure that involves the loading of propellants, and countdown to launch but without any actual firing of engines, so as to check out systems prior to the actual launch day"
    }
  ]
}