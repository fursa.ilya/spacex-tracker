package org.fursa.spacex.ui.models

class Past(
    val fightNumber: Int,
    val missionIconUrl: String,
    val missionName: String,
    val missionLaunchpad: String,
    val launchTime: String
)