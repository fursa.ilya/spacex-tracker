package org.fursa.spacex.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.acronym_item.view.*
import org.fursa.spacex.R

class AcronymAdapter : RecyclerView.Adapter<AcronymAdapter.AcronymViewHolder>(), Filterable {
    private var dataSource = mutableListOf<Acronym>()
    private var filteredList = mutableListOf<Acronym>()

    fun setAcronyms(dataSource: MutableList<Acronym>) {
        this.dataSource = dataSource
        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                if(charSequence?.isEmpty()!!) {
                    filteredList = dataSource
                } else {
                    val filtered = mutableListOf<Acronym>()
                    dataSource.filterTo(filtered) { it.title.toLowerCase().contains(charSequence.toString().toLowerCase()) }
                    dataSource = filtered
                }

                val filterRes = FilterResults()
                filterRes.values = filteredList
                return filterRes
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredList = results?.values as MutableList<Acronym>
                notifyDataSetChanged()
            }

        }
    }

    inner class AcronymViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(acronym: Acronym) = with(itemView) {
            txtTitle.text = acronym.title
            txtMeaning.text = acronym.meaning
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AcronymViewHolder {
        val acronymView = LayoutInflater.from(parent.context).inflate(R.layout.acronym_item, parent, false)
        return AcronymViewHolder(acronymView)
    }

    override fun getItemCount() = dataSource.size

    override fun onBindViewHolder(holder: AcronymViewHolder, position: Int) {
        val acronym = dataSource[position]
        holder.bind(acronym)
    }
}
