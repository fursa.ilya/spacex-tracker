package org.fursa.spacex.ui.fragments

import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.PopupMenu
import androidx.appcompat.widget.SearchView
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.fragment_upcoming.*
import org.fursa.spacex.ui.viewmodel.UpcomingViewModel
import org.fursa.spacex.R
import org.fursa.spacex.ui.adapter.UpcomingAdapter

class UpcomingFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private val upcomingMissionAdapter = UpcomingAdapter()
    private lateinit var viewModel: UpcomingViewModel

    companion object {
        fun newInstance(): UpcomingFragment {
            return UpcomingFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(this).get(UpcomingViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        viewModel.doLoad()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_upcoming, container, false)
    }

    override fun onRefresh() {
        viewModel.doLoad()
        upcomingRefresher.isRefreshing = false
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val searchItem = menu.findItem(R.id.search_action)
        val searchView = menu.findItem(R.id.search_action).actionView as SearchView

        searchView.queryHint = resources.getString(R.string.search_launch)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.onQueryTextSubmit(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.onQueryTextSubmit(newText)
                return true
            }

        })

        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                viewModel.doLoad()
                searchView.clearFocus()
                return true
            }
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.sort_action -> {
                showPopupMenu(requireView())
            }
        }

        return true
    }

    private fun showPopupMenu(view: View) {
        val popupMenu = PopupMenu(view.context, view)
        popupMenu.setOnMenuItemClickListener { item: MenuItem? ->  return@setOnMenuItemClickListener true }
        popupMenu.inflate(R.menu.sort_menu)
        popupMenu.gravity = GravityCompat.END
        popupMenu.show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(upcomingRecyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = upcomingMissionAdapter
        }

        (upcomingRecyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        upcomingRefresher.setOnRefreshListener(this)

        val onUpcomingItemClickListener = object: UpcomingAdapter.OnUpcomingItemClickListener {
            override fun onUpcomingItemClicked(flightNumber: Int?) {
                Toast.makeText(context, "id $flightNumber", Toast.LENGTH_LONG).show()
            }
        }

        upcomingMissionAdapter.setUpcomingCallback(onUpcomingItemClickListener)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.getFuture().observe(viewLifecycleOwner,
            Observer { missions ->
                upcomingMissionAdapter.setDataSource(missions)
                upcomingProgressBar.visibility = View.GONE
                upcomingRecyclerView.visibility = View.VISIBLE
                if(upcomingErrorView.visibility == View.VISIBLE) {
                    upcomingErrorView.visibility = View.GONE
                }
            })

        viewModel.getUpcomingError().observe(viewLifecycleOwner,
            Observer { error ->
                if(error == true) {
                    upcomingProgressBar.visibility = View.GONE
                    upcomingRecyclerView.visibility = View.GONE
                    upcomingErrorView.visibility = View.VISIBLE
                } else {
                    upcomingRecyclerView.visibility = View.VISIBLE
                    upcomingErrorView.visibility = View.GONE
                }
            })

    }
}
