package org.fursa.spacex.ui.tab.slider

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.viewpager.widget.PagerAdapter
import kotlinx.android.synthetic.main.flickr_item.view.*
import org.fursa.spacex.R
import org.fursa.spacex.utils.loadUrl

class SlidePagerAdapter : PagerAdapter {
    private var context: Context
    private var slides = mutableListOf<String>()

    constructor(context: Context, slides: MutableList<String>) {
        this.context = context
        this.slides = slides
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return slides.count()
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView((`object` as View))
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val sliderView = inflater.inflate(R.layout.flickr_item, null)
        val currentSlideItem = slides[position]
        with(sliderView) {
            imgFlickrSlide.loadUrl(currentSlideItem)
            imgFlickrSlide.visibility = View.VISIBLE
        }

        container.addView(sliderView)
        return sliderView
    }
}