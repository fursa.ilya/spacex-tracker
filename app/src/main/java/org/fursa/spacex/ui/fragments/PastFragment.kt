package org.fursa.spacex.ui.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.PopupMenu
import androidx.appcompat.widget.SearchView
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.fragment_past.*
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.R
import org.fursa.spacex.utils.const.ApiConst.FLIGHT_NUMBER
import org.fursa.spacex.utils.const.ApiConst.MISSION_NAME
import org.fursa.spacex.ui.adapter.PastAdapter
import org.fursa.spacex.ui.viewmodel.PastViewModel
import javax.inject.Inject

class PastFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private lateinit var viewModel: PastViewModel
    private val pastAdapter = PastAdapter()

    companion object {
        fun newInstance(): PastFragment {
            return PastFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(this).get(PastViewModel::class.java)

    }

    override fun onStart() {
        super.onStart()
        viewModel.doLoad()
    }

    override fun onRefresh() {
        viewModel.doLoad()
        pastRefresher.isRefreshing = false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_past, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val searchItem = menu.findItem(R.id.search_action)
        val searchView = menu.findItem(R.id.search_action).actionView as SearchView

        searchView.queryHint = resources.getString(R.string.search_launch)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.onQueryTextSubmit(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.onQueryTextSubmit(newText)
                return true
            }

        })

        searchItem.setOnActionExpandListener(object: MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                viewModel.doLoad()
                searchView.clearFocus()
                return true
            }
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.sort_action -> {
                showPopupMenu(requireView())
            }
        }

        return true
    }

    private fun showPopupMenu(view: View) {
        val popupMenu = PopupMenu(view.context, view)
        popupMenu.setOnMenuItemClickListener { item: MenuItem? ->  return@setOnMenuItemClickListener true }
        popupMenu.inflate(R.menu.sort_menu)
        popupMenu.gravity = GravityCompat.END
        popupMenu.show()


        //Todo refactor this code
        popupMenu.setOnMenuItemClickListener {
            when(it.itemId) {
                R.id.sort_asc -> {
                    viewModel.getCompleted().observe(this,
                        Observer { missions ->
                            pastAdapter.setDataSource(missions)
                            pastProgressBar.visibility = View.GONE
                            pastRecyclerView.visibility = View.VISIBLE
                            if(pastErrorView.visibility == View.VISIBLE) {
                                pastErrorView.visibility = View.GONE
                            }
                        })
                    return@setOnMenuItemClickListener true
                }

                else -> {
                    viewModel.getCompleted().observe(this,
                        Observer { missions ->
                            pastAdapter.setDataSource(missions.reversed())
                            pastProgressBar.visibility = View.GONE
                            pastRecyclerView.visibility = View.VISIBLE
                            if(pastErrorView.visibility == View.VISIBLE) {
                                pastErrorView.visibility = View.GONE
                            }
                        })
                    return@setOnMenuItemClickListener true
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(pastRecyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = pastAdapter
        }
        (pastRecyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        pastRefresher.setOnRefreshListener(this)

        val onPastItemClickListener = object: PastAdapter.OnPastItemClickListener {
            override fun onPastItemClicked(flightNumber: Int, missionName: String) {
                val args = Bundle().apply {
                    putInt(FLIGHT_NUMBER, flightNumber)
                    putString(MISSION_NAME, missionName)
                }

                val dialog = PastMissionFragment.newInstance(args)
                dialog.show(activity!!.supportFragmentManager, dialog.tag)
            }
        }

        pastAdapter.setPastCallback(onPastItemClickListener)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.getCompleted().observe(this,
            Observer { missions ->
                pastAdapter.setDataSource(missions)
                pastProgressBar.visibility = View.GONE
                pastRecyclerView.visibility = View.VISIBLE
                if(pastErrorView.visibility == View.VISIBLE) {
                    pastErrorView.visibility = View.GONE
                }
            })

        viewModel.getPastError().observe(this,
            Observer { error ->
                if(error == true) {
                    pastProgressBar.visibility = View.GONE
                    pastRecyclerView.visibility = View.GONE
                    pastErrorView.visibility = View.VISIBLE
                } else {
                    pastRecyclerView.visibility = View.VISIBLE
                    pastErrorView.visibility = View.GONE
                }
            })


    }

}

