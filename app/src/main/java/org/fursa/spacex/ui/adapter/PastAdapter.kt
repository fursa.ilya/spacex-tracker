package org.fursa.spacex.ui.adapter

import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.past_item.view.*
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.R
import org.fursa.spacex.api.model.past.PastResponse
import org.fursa.spacex.ui.models.Completed
import org.fursa.spacex.utils.formatDate
import org.fursa.spacex.utils.formatUiDate
import org.fursa.spacex.utils.loadUrl
import javax.inject.Inject

class PastAdapter : RecyclerView.Adapter<PastAdapter.PastViewHolder>() {
    private var dataSource = listOf<Completed>()
    private var pastItemClickListener: OnPastItemClickListener? = null

    init {
        AppDelegate.injector.inject(this)
    }

    @Inject
    lateinit var preferences: SharedPreferences

    private val enableAnimationKey = preferences.getBoolean("animation_enable_key", true)


    fun setDataSource(dataSource: List<Completed>) {
        this.dataSource = dataSource
        notifyDataSetChanged()
    }

    inner class PastViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(mission: Completed) = with(itemView){
            detailProgressBar.visibility = View.VISIBLE
            txtMissionName.text = mission.missionName
            txtFlightNumber.text = "#${mission.flightNumber}"
            txtLaunchTime.text = mission.launchTime
            txtDetails.text = mission.platformName
            mission.iconUrl.let {
                detailProgressBar.visibility = View.VISIBLE
                civMissionPatch.loadUrl(mission.iconUrl, detailProgressBar)
            }

            if(enableAnimationKey == true) {
                civMissionPatch.animation = AnimationUtils.loadAnimation(context, R.anim.fade_transition_animation)
                missionCard.animation = AnimationUtils.loadAnimation(context, R.anim.fade_transition_animation)
            }

            if(mission.launchSuccess) {
                imgSuccess.setImageResource(R.drawable.success_bullet)
            } else {
                imgSuccess.setImageResource(R.drawable.failed_bullet)
            }

            itemView.setOnClickListener {
                pastItemClickListener?.onPastItemClicked(mission.flightNumber, mission.missionName)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PastViewHolder {
        val pastView = LayoutInflater.from(parent.context).inflate(R.layout.past_item, parent, false)
        return PastViewHolder(pastView)
    }

    override fun getItemCount() = dataSource.count()

    override fun onBindViewHolder(holder: PastViewHolder, position: Int) {
        val pastMission = dataSource[position]
        holder.bind(pastMission)
    }

    fun setPastCallback(pastCallback: PastAdapter.OnPastItemClickListener) {
        this.pastItemClickListener = pastCallback
    }

    interface OnPastItemClickListener {
        fun onPastItemClicked(flightNumber: Int, missionName: String)
    }

}