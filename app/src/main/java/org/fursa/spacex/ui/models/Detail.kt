package org.fursa.spacex.ui.models

data class Detail(
    val missionName: String,
    val missionIconUrl: String,
    val missionLocation: String,
    val missionTime: String
)