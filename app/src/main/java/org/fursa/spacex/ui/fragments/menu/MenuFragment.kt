package org.fursa.spacex.ui.fragments.menu

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.menu_layout.*
import org.fursa.spacex.R
import org.fursa.spacex.ui.base.RoundedBottomSheet
import org.fursa.spacex.ui.fragments.AboutFragment
import org.fursa.spacex.ui.fragments.AcronymsFragment
import org.fursa.spacex.ui.settings.SettingsActivity

class MenuFragment : RoundedBottomSheet() {

    companion object {
        fun newInstance(): MenuFragment {
            return MenuFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.menu_layout, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        txtAcronyms.setOnClickListener {
            val acronymsFragment = AcronymsFragment.newInstance()
            acronymsFragment.show(activity!!.supportFragmentManager, acronymsFragment.tag)
            dismiss()
        }

        txtSettings.setOnClickListener {
            val settingsIntent = Intent(activity, SettingsActivity::class.java)
            startActivity(settingsIntent)
            dismiss()
        }
    }

}