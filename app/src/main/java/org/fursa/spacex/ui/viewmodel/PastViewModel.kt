package org.fursa.spacex.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.api.ApiRequest
import org.fursa.spacex.api.model.completed.BaseCompletedResponse
import org.fursa.spacex.api.model.completed.Platform
import org.fursa.spacex.api.model.past.PastResponse
import org.fursa.spacex.ui.models.Completed
import org.fursa.spacex.utils.mapper.CompletedMapper
import javax.inject.Inject

class PastViewModel(application: Application) : AndroidViewModel(application) {
   private val pastMissions = MutableLiveData<List<PastResponse>>()
   private val pastMissionsDesc = MutableLiveData<List<PastResponse>>()
   private val errorMessage = MutableLiveData<Boolean>()
   private val subs = CompositeDisposable()

   private val completedMissions = MutableLiveData<List<Completed>>()

   init {
       AppDelegate.injector.inject(this)
   }

   @Inject
   lateinit var apiRequest: ApiRequest

   @Inject
   lateinit var mapper: CompletedMapper

   fun doLoad() {
      subs.add(apiRequest.getCompleted()
         .subscribeOn(Schedulers.io())
         .observeOn(AndroidSchedulers.mainThread())
         .map { it -> mapper.map(it) }
         .subscribe(
            { missions ->
               completedMissions.value = missions
               errorMessage.value = false
            }, { _ ->
               errorMessage.value = true
            }))
   }

   fun getPastError() = errorMessage

   fun getCompleted() = completedMissions

   fun onQueryTextSubmit(missionTitle: String?) {
      val missions = completedMissions.value
      val result = if(missionTitle!!.isEmpty()) missions
      else missions!!.filter { it.missionName.contains(missionTitle, ignoreCase = true) }
      completedMissions.value = result
   }

   override fun onCleared() {
      super.onCleared()
      subs.clear()
   }
}