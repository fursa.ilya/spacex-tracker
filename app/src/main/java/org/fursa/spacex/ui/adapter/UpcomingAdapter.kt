package org.fursa.spacex.ui.adapter

import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.past_item.view.*
import kotlinx.android.synthetic.main.upcoming_item.view.*
import kotlinx.android.synthetic.main.upcoming_item.view.missionCard
import kotlinx.android.synthetic.main.upcoming_item.view.txtFlightNumber
import kotlinx.android.synthetic.main.upcoming_item.view.txtLaunchTime
import kotlinx.android.synthetic.main.upcoming_item.view.txtMissionName
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.R
import org.fursa.spacex.api.model.upcoming.UpcomingResponse
import org.fursa.spacex.ui.models.Future
import org.fursa.spacex.utils.formatDate
import org.fursa.spacex.utils.formatUiDate
import javax.inject.Inject

class UpcomingAdapter : RecyclerView.Adapter<UpcomingAdapter.UpcomingViewHolder>() {
    private var dataSource = listOf<Future>()
    private var upcomingItemClickListener: OnUpcomingItemClickListener? = null

    init {
        AppDelegate.injector.inject(this)
    }

    @Inject
    lateinit var preferences: SharedPreferences

    private val enableAnimationKey = preferences.getBoolean("animation_enable_key", true)

    fun setDataSource(dataSource: List<Future>) {
        this.dataSource = dataSource
        notifyDataSetChanged()
    }

    inner class UpcomingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(mission: Future) = with(itemView){
            txtMissionName.text = mission.missionName
            txtFlightNumber.text = "#${mission.flightNumber}"
            txtLaunchpad.text = mission.platformName
            txtLaunchTime.text = mission.launchTime

            itemView.setOnClickListener {
                upcomingItemClickListener?.onUpcomingItemClicked(mission.flightNumber)
            }

            if(enableAnimationKey == true) {
                missionCard.animation = AnimationUtils.loadAnimation(context, R.anim.fade_transition_animation)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpcomingViewHolder {
        val missionView = LayoutInflater.from(parent.context).inflate(R.layout.upcoming_item, parent, false)
        return UpcomingViewHolder(missionView)
    }

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onBindViewHolder(holder: UpcomingViewHolder, position: Int) {
        val mission = dataSource[position]
        holder.bind(mission)
    }

    fun setUpcomingCallback(onUpcomingItemClickListener: UpcomingAdapter.OnUpcomingItemClickListener) {
        this.upcomingItemClickListener = onUpcomingItemClickListener
    }

    interface OnUpcomingItemClickListener {
        fun onUpcomingItemClicked(flightNumber: Int?)
    }

}