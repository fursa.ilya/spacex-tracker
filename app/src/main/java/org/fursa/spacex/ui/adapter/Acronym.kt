package org.fursa.spacex.ui.adapter

data class Acronym(val title: String, val meaning: String): Item