package org.fursa.spacex.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_acronyms.*
import org.fursa.spacex.MainActivity
import org.fursa.spacex.R
import org.fursa.spacex.ui.adapter.Acronym
import org.fursa.spacex.ui.adapter.AcronymAdapter
import org.fursa.spacex.ui.adapter.FirstLetter
import org.fursa.spacex.ui.adapter.Item
import org.fursa.spacex.utils.getAcronyms

class AcronymsFragment : DialogFragment() {
    private val acronymsAdapter = AcronymAdapter()

    companion object {
        fun newInstance(): AcronymsFragment {
            return AcronymsFragment()
        }
    }
    //Todo переделать как в остальных фрагментах

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        (requireActivity() as MainActivity).setSupportActionBar(acronymsToolbar)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
    }

    override fun onStart() {
        super.onStart()
        val dialog = getDialog()
        if(dialog != null) {
            val dialogWidth = ViewGroup.LayoutParams.MATCH_PARENT
            val dialogHeight = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(dialogWidth, dialogHeight)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_acronyms, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val acronyms = getAcronyms(requireContext()).toMutableList().sortedBy { it.title }
        acronymsAdapter.setAcronyms(acronyms.toMutableList())

        with(acronymRecyclerView) {
            adapter = acronymsAdapter
            layoutManager = LinearLayoutManager(context)
        }

        acronymsToolbar.setNavigationOnClickListener { dismiss() }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.acronym_menu, menu)
        val searchItem = menu.findItem(R.id.search_action)
        val searchView = menu.findItem(R.id.search_action).actionView as SearchView

        searchView.queryHint = resources.getString(R.string.search_launch)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                val acronyms = getAcronyms(requireContext()).toMutableList().sortedBy { it.title }
                acronymsAdapter.setAcronyms(acronyms.toMutableList())
                acronymsAdapter.filter.filter(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                acronymsAdapter.filter.filter(newText)
                return true
            }

        })

        searchItem.setOnActionExpandListener(object: MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                searchView.clearFocus()
                val acronyms = getAcronyms(requireContext()).toMutableList().sortedBy { it.title }
                acronymsAdapter.setAcronyms(acronyms.toMutableList())
                return true
            }
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

}