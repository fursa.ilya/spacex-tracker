package org.fursa.spacex.ui.models

data class Future(
    val flightNumber: Int,
    val missionName: String,
    val platformName: String,
    val launchTime: String
)