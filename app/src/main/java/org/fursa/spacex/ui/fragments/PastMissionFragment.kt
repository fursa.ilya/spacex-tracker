package org.fursa.spacex.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_mission_details.*
import kotlinx.android.synthetic.main.detail_head_layout.*
import kotlinx.android.synthetic.main.flickr_item.*
import kotlinx.android.synthetic.main.payload_layout.*
import kotlinx.android.synthetic.main.rocket_layout.*
import org.fursa.spacex.MainActivity
import org.fursa.spacex.R
import org.fursa.spacex.utils.const.ApiConst.FLIGHT_NUMBER
import org.fursa.spacex.utils.const.ApiConst.MISSION_NAME
import org.fursa.spacex.ui.tab.slider.SlidePagerAdapter
import org.fursa.spacex.ui.viewmodel.DetailViewModel
import org.fursa.spacex.utils.formatDate
import org.fursa.spacex.utils.formatUiDate
import org.fursa.spacex.utils.loadUrl
import java.util.*

class PastMissionFragment : DialogFragment() {
    private var flightNumber = 0
    private val flickrSlides = mutableListOf<String>()
    private lateinit var viewModel: DetailViewModel

    private var youtubeUrl = ""

    companion object {
        fun newInstance(args: Bundle): PastMissionFragment {
            val fragment = PastMissionFragment()
            val args = args
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity() as MainActivity).setSupportActionBar(missionToolbar)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
        setHasOptionsMenu(true)
        flightNumber = arguments?.getInt(FLIGHT_NUMBER)!!

        viewModel = ViewModelProviders.of(this).get(DetailViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        val dialog = getDialog()
        if(dialog != null) {
            val dialogWidth = ViewGroup.LayoutParams.MATCH_PARENT
            val dialogHeight = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(dialogWidth, dialogHeight)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_mission_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        missionToolbar.title = arguments?.getString(MISSION_NAME)
        missionToolbar.setNavigationOnClickListener { dismiss() }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val flightNumber = arguments!!.getInt(FLIGHT_NUMBER)
        progressLayout.visibility = View.VISIBLE
        viewModel.doLoad(flightNumber)

        Toast.makeText(context, "Got flight number: $flightNumber", Toast.LENGTH_LONG).show()
        val sliderPagerAdapter = SlidePagerAdapter(requireContext(), flickrSlides)

        viewModel.getSliderImages().observe(this, androidx.lifecycle.Observer {
            if(it.isNotEmpty()) {
                flickrSlides.addAll(it)
                sliderPagerAdapter.notifyDataSetChanged()
                sliderProgressBar.visibility = View.GONE
                slider_pager.visibility = View.VISIBLE
            } else {
                sliderProgressBar.visibility = View.GONE
                slider_pager.visibility = View.GONE
                collapsingToolbar.background = ContextCompat.getDrawable(requireContext(), R.drawable.toolbar_background)
            }
        })

        collapsingToolbar.setOnClickListener { Toast.makeText(context, arguments?.getString(MISSION_NAME), Toast.LENGTH_LONG).show() }
        slider_pager.setOnClickListener { Toast.makeText(context, arguments?.getString(MISSION_NAME), Toast.LENGTH_LONG).show() }

        slider_pager.adapter = sliderPagerAdapter

        val timer = Timer()
        val timerTask = SliderTimerTask()
        timer.scheduleAtFixedRate(timerTask, 1000, 45_000)
        progressLayout.visibility = View.VISIBLE
        contentLayout.visibility = View.GONE

        viewModel.getDetail().observe(this, androidx.lifecycle.Observer { launch ->

            launch.forEach { detail ->
                txtMissionName.text = detail.missionName
                civMissionPatch.loadUrl(detail.links.missionPatch, detailProgressBar)
                txtTime.text = formatUiDate(detail.launchDateUnix.toLong())
                txtPlatform.text = detail.launchSite.siteName

                if(detail.details.isNullOrEmpty()) {
                    txtDetails.text = resources.getString(R.string.mission_has_no_details)
                } else {
                    txtDetails.text = detail.details
                }

                txtModel.text = detail.rocket.rocketName
                txtStaticFire.text = formatUiDate(detail.staticFireDateUnix.toLong())

                if(detail.launchSuccess) {
                    imgLaunchSuccess.setImageResource(R.drawable.success_bullet)
                } else {
                    imgLaunchSuccess.setImageResource(R.drawable.failed_bullet)
                }

                if(detail.rocket.firstStage.cores[0].reused) {
                    imgReused.setImageResource(R.drawable.success_bullet)
                } else {
                    imgReused.setImageResource(R.drawable.failed_bullet)
                }

                if(detail.rocket.firstStage.cores[0].landingVehicle.isNullOrEmpty()) {
                    txtLandingVehicle.text = resources.getString(R.string.landing_vehicle_unknown)
                } else {
                    txtLandingVehicle.text = detail.rocket.firstStage.cores[0].landingVehicle
                }

                txtPayloadName.text = detail.rocket.secondStage.payloads[0].payloadId
                txtPayloadNationality.text = detail.rocket.secondStage.payloads[0].nationality
                txtManufacturer.text = detail.rocket.secondStage.payloads[0].manufacturer
                txtCustomer.text = detail.rocket.secondStage.payloads[0].customers[0]
                txtWeight.text = detail.rocket.secondStage.payloads[0].massReturnedKg.toString()


                youtubeUrl = detail.links.videoLink
            }


            progressLayout.visibility = View.GONE
            contentLayout.visibility = View.VISIBLE
        })


        fabWatch.setOnClickListener {
            if(youtubeUrl.isNotEmpty() and youtubeUrl.isNotBlank()) {
               watchYoutube(youtubeUrl)
            }
        }


    }

    private fun watchYoutube(youtubeUrl: String) {
        val watchIntent = Intent(Intent.ACTION_VIEW)
        watchIntent.data = Uri.parse(youtubeUrl)
        startActivity(watchIntent)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.detail_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.watch_action -> { Toast.makeText(context, "Watch", Toast.LENGTH_LONG).show() }
        }

        return true
    }

    inner class SliderTimerTask : TimerTask() {
        override fun run() {
           if(activity != null) {
               (requireActivity() as MainActivity).runOnUiThread {
                   if(slider_pager.currentItem < flickrSlides.size - 1) {
                       slider_pager.currentItem = slider_pager.currentItem + 1
                   } else {
                       slider_pager.currentItem = 0
                   }
               }
           }
        }

    }
}