package org.fursa.spacex.ui.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.api.ApiRequest
import org.fursa.spacex.api.model.future.BaseFutureResponse
import org.fursa.spacex.api.model.future.LaunchSite
import org.fursa.spacex.api.model.next.NextResponse
import org.fursa.spacex.api.model.upcoming.UpcomingResponse
import org.fursa.spacex.ui.models.Future
import org.fursa.spacex.utils.mapper.FutureMapper
import javax.inject.Inject

class UpcomingViewModel(application: Application) : AndroidViewModel(application) {
    private val upcomingMissions = MutableLiveData<List<UpcomingResponse>>()
    private val futureMissions = MutableLiveData<List<Future>>()
    private val nextLaunch = MutableLiveData<NextResponse>()
    private val errorMessage = MutableLiveData<Boolean>()
    private val subs = CompositeDisposable()

    init {
        AppDelegate.injector.inject(this)
    }

    @Inject
    lateinit var apiRequest: ApiRequest

    @Inject
    lateinit var mapper: FutureMapper

    fun doLoad() {
       subs.add(apiRequest.getFuture()
           .subscribeOn(Schedulers.io())
           .observeOn(AndroidSchedulers.mainThread())
           .map { it -> mapper.map(it) }
           .subscribe(
               { missions ->
                   futureMissions.value = missions
                   errorMessage.value = false
               }, { error ->
                   errorMessage.value = true
               }))

        subs.add(apiRequest.getNextKnownLaunch()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { launch -> nextLaunch.value = launch },
                { error -> Log.d("Next launch", "${error.localizedMessage}")})
        )
    }

    @Deprecated("Remove if getFuture() works")
    fun getUpcoming() = upcomingMissions

    fun getFuture() = futureMissions

    fun getUpcomingError() = errorMessage

    fun getNextKnownLaunch() = nextLaunch

    override fun onCleared() {
        super.onCleared()
        subs.clear()
    }

    fun onQueryTextSubmit(missionTitle: String?) {
        val missions = futureMissions.value
        val result = if(missionTitle!!.isEmpty()) missions
        else missions!!.filter { it.missionName.contains(missionTitle, ignoreCase = true) }
        futureMissions.value = result
    }
}