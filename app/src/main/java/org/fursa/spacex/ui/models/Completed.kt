package org.fursa.spacex.ui.models

data class Completed(
    val iconUrl: String,
    val flightNumber: Int,
    val launchSuccess: Boolean,
    val missionName: String,
    val platformName: String,
    val launchTime: String
)