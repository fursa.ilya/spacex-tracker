package org.fursa.spacex.ui.settings

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import kotlinx.android.synthetic.main.settings_activity.*
import org.fursa.spacex.R
import org.fursa.spacex.ui.fragments.AboutFragment

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment())
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setSupportActionBar(toolbarSettings)

        toolbarSettings.setNavigationOnClickListener { finish() }
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.preferences, rootKey)

            val emailPreference = findPreference<Preference>("email_key")
            val aboutPreference = findPreference<Preference>("about_key")

            emailPreference?.setOnPreferenceClickListener {
                Toast.makeText(context, "Test", Toast.LENGTH_LONG).show()
                return@setOnPreferenceClickListener true
            }

            aboutPreference?.setOnPreferenceClickListener {
                val aboutFragment = AboutFragment.newInstance()
                aboutFragment.show(activity!!.supportFragmentManager, aboutFragment.tag)
                return@setOnPreferenceClickListener true
            }

        }
    }
}