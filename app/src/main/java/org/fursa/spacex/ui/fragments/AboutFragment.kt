package org.fursa.spacex.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.fragment_about.*
import org.fursa.spacex.R
import org.fursa.spacex.ui.settings.SettingsActivity


class AboutFragment : DialogFragment() {
    companion object {
        fun newInstance(): AboutFragment {
            return AboutFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        (requireActivity() as SettingsActivity).setSupportActionBar(aboutToolbar)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
    }

    override fun onStart() {
        super.onStart()
        val dialog = getDialog()
        if(dialog != null) {
            val dialogWidth = ViewGroup.LayoutParams.MATCH_PARENT
            val dialogHeight = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(dialogWidth, dialogHeight)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        aboutToolbar.title = resources.getString(R.string.about)
        aboutToolbar.setNavigationOnClickListener { dismiss() }

        imgDonate.setOnClickListener {
            val url = "https://www.buymeacoffee.com/KvSH5Nk"
            val donateIntent = Intent(Intent.ACTION_VIEW)
            donateIntent.data = Uri.parse(url)
            startActivity(donateIntent)
        }
    }
}