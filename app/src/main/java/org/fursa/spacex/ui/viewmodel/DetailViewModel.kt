package org.fursa.spacex.ui.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.fursa.spacex.data.network.model.mission.MissionResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.api.ApiRequest
import javax.inject.Inject

class DetailViewModel(application: Application) : AndroidViewModel(application) {
    private val missionDetail = MutableLiveData<List<MissionResponse>>()
    private val sliderImages = MutableLiveData<List<String>>()
    private val subs = CompositeDisposable()
    private val errorMessage = MutableLiveData<Boolean>()

    init {
        AppDelegate.injector.inject(this)
    }

    @Inject
    lateinit var apiRequest: ApiRequest

    fun doLoad(flightNumber: Int) {
        subs.add(
            apiRequest.getLaunch(flightNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { Log.d("DetailViewModel", "subscribed") }
                .subscribe( { missions ->
                    missionDetail.value = missions
                    errorMessage.value = false
                    sliderImages.value = missions[0].links.flickrImages
                }, { error ->
                    errorMessage.value = true
                }))
    }

    fun getSliderImages(): MutableLiveData<List<String>> {
        return sliderImages
    }

    fun getDetail() = missionDetail

    override fun onCleared() {
        super.onCleared()
        subs.clear()
    }
}