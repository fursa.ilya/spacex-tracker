package org.fursa.spacex.api.model.future

import com.google.gson.annotations.SerializedName

class BaseFutureResponse <T> {
    @SerializedName("flight_number")
    val flightNumber: Int = 0
    @SerializedName("mission_name")
    val missionName: String = ""
    @SerializedName("launch_date_unix")
    val launchDateUnix: Int = 0
    @SerializedName("launch_site")
    val launchSite: T? = null
}