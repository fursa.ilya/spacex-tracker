package org.fursa.spacex.api.model.completed

import com.google.gson.annotations.SerializedName

data class Icon(
    @SerializedName("mission_patch_small")
    val iconUrl: String
)