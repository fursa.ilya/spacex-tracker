package org.fursa.spacex.api.model.past

data class FirstStage(
    val cores: List<Core>
)