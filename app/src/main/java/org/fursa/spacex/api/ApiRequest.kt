package org.fursa.spacex.api

import com.fursa.spacex.data.network.model.mission.MissionResponse
import io.reactivex.Single
import org.fursa.spacex.api.model.completed.BaseCompletedResponse
import org.fursa.spacex.api.model.completed.Platform
import org.fursa.spacex.api.model.future.BaseFutureResponse
import org.fursa.spacex.api.model.future.LaunchSite
import org.fursa.spacex.utils.const.ApiConst.FLIGHT_NUMBER
import org.fursa.spacex.api.model.next.NextResponse
import org.fursa.spacex.api.model.past.PastResponse
import org.fursa.spacex.api.model.upcoming.UpcomingResponse
import org.fursa.spacex.utils.const.ApiConst
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiRequest {

    @Deprecated("Will be deleted after creating better response structure")
    @GET(ApiConst.GET_UPCOMING_LAUNCHES)
    fun getUpcoming(): Single<List<UpcomingResponse>>

    @GET(ApiConst.GET_PAST_LAUNCHES)
    fun getPast(): Single<List<PastResponse>>

    @GET(ApiConst.GET_LAUNCH)
    fun getLaunch(@Query(FLIGHT_NUMBER) flightNumber: Int): Single<List<MissionResponse>>

    @GET(ApiConst.GET_NEXT_KNOWN_LAUNCH)
    fun getNextKnownLaunch(): Single<NextResponse>

    @GET(ApiConst.GET_UPCOMING_LAUNCHES)
    fun getFuture(): Single<List<BaseFutureResponse<LaunchSite>>>

    @GET(ApiConst.GET_PAST_LAUNCHES)
    fun getCompleted(): Single<List<BaseCompletedResponse<Platform>>>

}