package com.fursa.spacex.data.network.model.mission


data class FirstStage(
    val cores: List<Core>
)