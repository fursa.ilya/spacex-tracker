package org.fursa.spacex.api.model.next


import com.google.gson.annotations.SerializedName

data class Fairings(
    @SerializedName("recovered")
    val recovered: Any?,
    @SerializedName("recovery_attempt")
    val recoveryAttempt: Boolean?,
    @SerializedName("reused")
    val reused: Boolean?,
    @SerializedName("ship")
    val ship: Any?
)