package org.fursa.spacex.api.model.past


data class LaunchFailureDetails(
    val altitude: Int,
    val reason: String,
    val time: Int
)