package org.fursa.spacex.api.model.future

import com.google.gson.annotations.SerializedName

class LaunchSite {
    @SerializedName("site_name")
    val siteName: String = ""
}