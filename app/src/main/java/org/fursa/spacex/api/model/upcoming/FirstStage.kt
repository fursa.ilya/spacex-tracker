package org.fursa.spacex.api.model.upcoming


import com.google.gson.annotations.SerializedName

data class FirstStage(
    @SerializedName("cores")
    val cores: List<Core?>?
)