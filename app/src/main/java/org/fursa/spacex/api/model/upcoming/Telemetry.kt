package org.fursa.spacex.api.model.upcoming


import com.google.gson.annotations.SerializedName

data class Telemetry(
    @SerializedName("flight_club")
    val flightClub: Any?
)