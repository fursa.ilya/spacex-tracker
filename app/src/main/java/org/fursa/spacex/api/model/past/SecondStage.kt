package org.fursa.spacex.api.model.past


data class SecondStage(
    val block: Int,
    val payloads: List<Payload>
)