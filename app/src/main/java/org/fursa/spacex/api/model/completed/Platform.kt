package org.fursa.spacex.api.model.completed

import com.google.gson.annotations.SerializedName

class Platform {
    @SerializedName("site_name")
    val siteName: String = ""
}