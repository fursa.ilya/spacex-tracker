package org.fursa.spacex.api.model.completed

import com.google.gson.annotations.SerializedName

class BaseCompletedResponse <T> {
    @SerializedName("flight_number")
    val flightNumber: Int = 0
    @SerializedName("mission_name")
    val missionName: String = ""
    @SerializedName("launch_date_unix")
    val launchDateUnix: Int = 0
    @SerializedName("launch_success")
    val launchSuccess: Boolean = false
    @SerializedName("launch_site")
    val launchSite: T? = null
    @SerializedName("links")
    val icon: Icon? = null
}