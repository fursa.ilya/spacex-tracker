package org.fursa.spacex.api.model.next


import com.google.gson.annotations.SerializedName

data class FirstStage(
    @SerializedName("cores")
    val cores: List<Core?>?
)