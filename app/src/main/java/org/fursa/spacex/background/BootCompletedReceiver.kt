package org.fursa.spacex.background

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import androidx.work.*
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.utils.const.ApiConst.ALARM_WORKER_TAG
import org.fursa.spacex.utils.const.ApiConst.DATABASE_WORKER_TAG
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BootCompleteReceiver : BroadcastReceiver() {

    init {
        AppDelegate.injector.inject(this)
    }

    @Inject
    lateinit var preferences: SharedPreferences

    override fun onReceive(ctx: Context?, intent: Intent?) {
        if(Intent.ACTION_BOOT_COMPLETED == intent?.action) {
            Log.d("Spacex/Boot", "onReceived")

            val dbRequest = PeriodicWorkRequest
                .Builder(LaunchInfoUpdateWorker::class.java, 1, TimeUnit.DAYS)
                .addTag(DATABASE_WORKER_TAG)
                .build()
            WorkManager.getInstance().enqueue(dbRequest)


            val alarmRequest = OneTimeWorkRequest
                .Builder(AlarmSetterWorker::class.java)
                .addTag(ALARM_WORKER_TAG)
                .build()
            WorkManager.getInstance().enqueue(alarmRequest)
        }

    }

}
