package org.fursa.spacex.background

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.utils.const.ApiConst.MISSION_DETAIL
import org.fursa.spacex.utils.const.ApiConst.MISSION_NAME
import org.fursa.spacex.api.ApiRequest
import org.fursa.spacex.db.Launch
import org.fursa.spacex.utils.formatDate
import org.fursa.spacex.utils.mapper.LaunchMapper
import java.util.*
import javax.inject.Inject

class AlarmSetterWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {

    init {
        AppDelegate.injector.inject(this)
    }

    @Inject
    lateinit var alarmManager: AlarmManager

    @Inject
    lateinit var apiRequest: ApiRequest

    @Inject
    lateinit var preferences: SharedPreferences

    @Inject
    lateinit var mapper: LaunchMapper

    private val disposable = CompositeDisposable()

    private val rightBeforeKey = preferences.getBoolean("just_before_key", false)
    private val dayBeforeKey = preferences.getBoolean("day_before_key", false)

    override fun doWork(): Result {
        Log.d("Spacex/Alarm", "Right before key: $rightBeforeKey")
        Log.d("Spacex/Alarm", "Day before key: $dayBeforeKey")

        disposable.add(apiRequest.getNextKnownLaunch()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { launch -> mapper.map(launch) }
            .subscribe({ launch ->
                if(rightBeforeKey == true) { setRightBeforeAlarm(launch) }
                if(dayBeforeKey == true) { setDayBeforeAlarm(launch) }
            }, {
                Log.d("Spacex/Alarm", it.localizedMessage)
            }))
        return Result.success()
    }

    private fun setRightBeforeAlarm(launch: Launch) {
        val calendar = Calendar.getInstance()
        calendar.apply { timeInMillis = launch.timestamp * 1000 }
        val alarmIntent = Intent(applicationContext, AlarmReceiver::class.java)
        alarmIntent.putExtra(MISSION_NAME, launch.title)
        alarmIntent.putExtra(MISSION_DETAIL, "Is launching right now!")
        val pendingIntent = PendingIntent.getBroadcast(
            applicationContext,
            0,
            alarmIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        Log.d("Spacex/Alarm", "Right before set at: ${formatDate(calendar.timeInMillis)}")
        when {
            Build.VERSION.SDK_INT >= 23 -> {
                alarmManager.setExactAndAllowWhileIdle(
                    AlarmManager.RTC_WAKEUP,
                    calendar.timeInMillis, pendingIntent
                );
            }
            Build.VERSION.SDK_INT >= 19 -> {
                alarmManager.setExact(
                    AlarmManager.RTC_WAKEUP,
                    calendar.timeInMillis, pendingIntent
                );
            }
            else -> {
                alarmManager.set(
                    AlarmManager.RTC_WAKEUP,
                    calendar.timeInMillis, pendingIntent
                );
            }
        }
    }

    private fun setDayBeforeAlarm(launch: Launch) {
        val calendar = Calendar.getInstance()
        calendar.apply { timeInMillis = launch.timestamp * 1000 }
        calendar.add(Calendar.DAY_OF_MONTH, -1)
        val alarmIntent = Intent(applicationContext, AlarmReceiver::class.java)
        alarmIntent.putExtra(MISSION_NAME, launch.title)
        alarmIntent.putExtra(MISSION_DETAIL, "Is launching tomorrow!")
        val pendingIntent = PendingIntent.getBroadcast(applicationContext, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        Log.d("Spacex/Alarm", "Day before set at: ${formatDate(calendar.timeInMillis)}")

        when {
            Build.VERSION.SDK_INT >= 23 -> {
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    calendar.timeInMillis, pendingIntent);
            }
            Build.VERSION.SDK_INT >= 19 -> {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                    calendar.timeInMillis, pendingIntent);
            }
            else -> {
                alarmManager.set(AlarmManager.RTC_WAKEUP,
                    calendar.timeInMillis, pendingIntent);
            }
        }
    }

    override fun onStopped() {
        super.onStopped()
        disposable.clear()
    }
}
