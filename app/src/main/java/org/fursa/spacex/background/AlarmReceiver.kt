package org.fursa.spacex.background

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.utils.const.ApiConst.MISSION_DETAIL
import org.fursa.spacex.utils.const.ApiConst.MISSION_NAME
import javax.inject.Inject

class AlarmReceiver : BroadcastReceiver() {

    init {
        AppDelegate.injector.inject(this)
    }

    @Inject
    lateinit var notificationHelper: NotificationHelper

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onReceive(context: Context?, intent: Intent?) {
        val title = intent?.extras?.getString(MISSION_NAME)
        val details = intent?.extras?.getString(MISSION_DETAIL)
        if (context != null) {
            notificationHelper.showNotification(title.toString(), details.toString(), context)
        }
    }
}