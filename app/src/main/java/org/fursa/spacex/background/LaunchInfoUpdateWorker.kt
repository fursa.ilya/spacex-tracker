package org.fursa.spacex.background

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.api.ApiRequest
import org.fursa.spacex.utils.const.ApiConst
import org.fursa.spacex.db.repository.LaunchRepository
import org.fursa.spacex.utils.mapper.LaunchMapper
import javax.inject.Inject


class LaunchInfoUpdateWorker(context: Context, workerParams: WorkerParameters): Worker(context, workerParams) {
    private val disposable = CompositeDisposable()

    init {
        AppDelegate.injector.inject(this)
    }

    @Inject
    lateinit var api: ApiRequest

    @Inject
    lateinit var repository: LaunchRepository

    @Inject
    lateinit var mapper: LaunchMapper

    override fun doWork(): Result {
        disposable.add(api.getNextKnownLaunch()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .filter {
                it.tentativeMaxPrecision == ApiConst.TENTATIVE_PRECISION_HOUR
            }
            .map { launch -> mapper.map(launch) }
            .subscribe(
                { launch -> repository.upsert(launch) },
                { error-> error(error.localizedMessage)}))
        return Result.success()
    }

    private fun error(message: String) {
        Log.d("Spacex/DbWorker", message)
    }

    override fun onStopped() {
        super.onStopped()
        disposable.clear()
    }
}
