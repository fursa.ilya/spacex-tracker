package org.fursa.spacex.background

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.MainActivity
import org.fursa.spacex.R
import org.fursa.spacex.utils.const.ApiConst.CHANNEL_ID
import javax.inject.Inject


@RequiresApi(Build.VERSION_CODES.O)
class NotificationHelper(context: Context) : ContextWrapper(context) {

    @Inject
    lateinit var notificationManager: NotificationManager

    @Inject
    lateinit var channel: NotificationChannel

    init {
        AppDelegate.injector.inject(this)

        channel.enableLights(true)
        channel.enableVibration(true)
        channel.lightColor = Color.GREEN
        channel.vibrationPattern =
            longArrayOf(100, 200, 300, 400, 500, 400, 500, 200, 500)
        notificationManager.createNotificationChannel(channel)
    }

    fun showNotification(title: String, details: String, context: Context) {
        val mainActivityIntent = Intent(context, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, mainActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val  notificationBuilder = NotificationCompat.Builder(
            context.applicationContext, CHANNEL_ID
        )
        notificationBuilder.setContentTitle(title)
        notificationBuilder.setContentText(details)
        notificationBuilder.setContentIntent(pendingIntent)
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
        notificationBuilder.setAutoCancel(true)

        val notification = notificationBuilder.build()
        notificationManager.notify(0, notification)
    }

    companion object {
        fun getInstance(context: Context): NotificationHelper {
            return NotificationHelper(context)
        }
    }

}