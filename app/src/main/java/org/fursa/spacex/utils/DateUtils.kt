@file:JvmName("DateUtils")

package org.fursa.spacex.utils

import android.os.Build
import androidx.annotation.RequiresApi
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*


fun formatUiDate(stamp: Long?): String {
    val dateFormat = SimpleDateFormat("dd MMMM yyyy HH:mm")
    return dateFormat.format(Timestamp(stamp?.times(1000)!!))
}

fun formatDate(stamp: Long?): String {
    val dateFormat = SimpleDateFormat("dd MMMM yyyy HH:mm")
    return dateFormat.format(stamp)
}


fun formatUnknownDate(stamp: Long?): String {
    val dateFormat = SimpleDateFormat("MMMM yyyy")
    return dateFormat.format(Timestamp(stamp?.times(1000)!!))
}

fun Long.toDate(): Date {
    return Date(this)
}
