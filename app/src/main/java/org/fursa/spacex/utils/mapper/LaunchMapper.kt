package org.fursa.spacex.utils.mapper

import org.fursa.spacex.api.model.next.NextResponse
import org.fursa.spacex.db.Launch

class LaunchMapper: AbstractMapper<NextResponse, Launch> {

    override fun map(response: NextResponse): Launch {
        return Launch(
            title = response.missionName.toString(),
            details = "Watch now!",
            timestamp = response.launchDateUnix.toLong(),
            flightNumber = response.flightNumber!!
        )
    }
}