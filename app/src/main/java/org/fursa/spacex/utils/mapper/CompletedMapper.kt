package org.fursa.spacex.utils.mapper

import org.fursa.spacex.api.model.completed.BaseCompletedResponse
import org.fursa.spacex.api.model.completed.Platform
import org.fursa.spacex.ui.models.Completed
import org.fursa.spacex.utils.formatUiDate

class CompletedMapper : AbstractMapper<List<BaseCompletedResponse<Platform>>, List<Completed>> {
    override fun map(input: List<BaseCompletedResponse<Platform>>): List<Completed> {
        val result = mutableListOf<Completed>()
        input.forEach {
            result.add(
                Completed(
                it.icon?.iconUrl.toString(),
                it.flightNumber,
                it.launchSuccess,
                it.missionName,
                it.launchSite?.siteName.toString(),
                formatUiDate(it.launchDateUnix.toLong()))
            )
        }

        return result
    }

}