package org.fursa.spacex.utils

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun ImageView.loadUrl(url: String, progressBar: ProgressBar) {
    Picasso.get()
        .load(url)
        .into(this, object : Callback {
            override fun onSuccess() {
                progressBar.visibility = View.GONE
                this@loadUrl.visibility = View.VISIBLE
            }

            override fun onError(e: Exception?) {
                progressBar.visibility = View.VISIBLE
                this@loadUrl.visibility = View.GONE
            }
        })
}

fun ImageView.loadUrl(url: String) {
    Picasso.get()
        .load(url)
        .into(this)
}
