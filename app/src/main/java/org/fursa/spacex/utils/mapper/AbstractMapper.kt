package org.fursa.spacex.utils.mapper

interface AbstractMapper<I, O> {
    fun map(input: I): O
}