package org.fursa.spacex.utils.mapper

import org.fursa.spacex.api.model.past.PastResponse
import org.fursa.spacex.ui.models.Past
import org.fursa.spacex.utils.formatUiDate

class PastMapper : AbstractMapper<List<PastResponse>, List<Past>> {

    override fun map(past: List<PastResponse>): List<Past> {
        val pastMissions = mutableListOf<Past>()
        past.forEach {
            pastMissions.add(
                Past(
                fightNumber = it.flightNumber,
                missionIconUrl = it.links.missionPatch,
                missionName = it.missionName,
                missionLaunchpad = it.launchSite.siteNameLong,
                launchTime = formatUiDate(it.launchDateUnix)
            ))
        }

        return pastMissions
    }

}