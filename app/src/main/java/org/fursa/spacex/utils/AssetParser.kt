package org.fursa.spacex.utils

import android.content.Context
import org.json.JSONObject
import org.fursa.spacex.ui.adapter.Acronym

fun fileToString(filename: String, context: Context): String {
    val appContext = context.applicationContext
    return appContext.assets
        .open(filename)
        .bufferedReader()
        .use { it.readText() }
}

fun getAcronyms(context: Context): List<Acronym> {
    val acronyms = mutableListOf<Acronym>()
    val jsonString = fileToString("acronyms.json", context)
    val jsonObj = JSONObject(jsonString)
    val acronymsArr = jsonObj.getJSONArray("acronyms")

    for(i in 0 until acronymsArr.length()) {
        val currentObject = acronymsArr.getJSONObject(i)
        val acronym = currentObject.getString("acronym")
        val meaning = currentObject.getString("meaning")
        val result = Acronym(acronym, meaning)
        acronyms.add(result)
    }

    return acronyms
}