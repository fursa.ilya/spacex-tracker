package org.fursa.spacex.utils.mapper

import org.fursa.spacex.api.model.future.BaseFutureResponse
import org.fursa.spacex.api.model.future.LaunchSite
import org.fursa.spacex.ui.models.Future
import org.fursa.spacex.utils.formatUiDate

class FutureMapper : AbstractMapper<List<BaseFutureResponse<LaunchSite>>, List<Future>> {
    override fun map(input: List<BaseFutureResponse<LaunchSite>>): List<Future> {
        val result = mutableListOf<Future>()
        input.forEach {
            result.add(Future(
                it.flightNumber,
                it.missionName,
                it.launchSite?.siteName.toString(),
                formatUiDate(it.launchDateUnix.toLong())))
        }

        return result
    }

}