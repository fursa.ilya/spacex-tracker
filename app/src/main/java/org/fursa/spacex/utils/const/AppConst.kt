package org.fursa.spacex.utils.const

object ApiConst {
    const val BASE_URL = "https://api.spacexdata.com/v3/"
    const val GET_UPCOMING_LAUNCHES = "launches/upcoming"
    const val GET_PAST_LAUNCHES = "launches/past"
    const val GET_LAUNCH = "launches/"
    const val GET_NEXT_KNOWN_LAUNCH = "launches/next"

    const val FLIGHT_NUMBER = "flight_number"
    const val MISSION_NAME = "mission_name"
    const val MISSION_DETAIL = "mission_detail"
    const val TENTATIVE_PRECISION_HOUR = "hour"

    const val DATABASE_NAME = "space.db"
    const val DATABASE_WORKER_TAG = "db-worker"
    const val ALARM_WORKER_TAG = "alarm-worker"

    const val CHANNEL_ID = "spacex-notifications"
    const val CHANNEL_NAME = "spacex-launch-tracker"
}