package org.fursa.spacex.di

import dagger.Module
import dagger.Provides
import org.fursa.spacex.utils.mapper.CompletedMapper
import org.fursa.spacex.utils.mapper.FutureMapper
import org.fursa.spacex.utils.mapper.LaunchMapper
import org.fursa.spacex.utils.mapper.PastMapper
import javax.inject.Singleton

@Module
class MapperModule {

    @Singleton
    @Provides
    fun provideLaunchMapper(): LaunchMapper = LaunchMapper()

    @Singleton
    @Provides
    fun providePastMapper(): PastMapper = PastMapper()

    @Singleton
    @Provides
    fun provideFutureMapper(): FutureMapper = FutureMapper()

    @Singleton
    @Provides
    fun provideCompletedMapper(): CompletedMapper = CompletedMapper()


}