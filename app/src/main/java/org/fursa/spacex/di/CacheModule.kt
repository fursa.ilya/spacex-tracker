package org.fursa.spacex.di

import android.content.Context
import dagger.Module
import dagger.Provides
import org.fursa.spacex.db.AppDatabase
import org.fursa.spacex.db.repository.LaunchRepository
import javax.inject.Singleton

@Module
class CacheModule(appContext: Context) {
    private val context = appContext
    private val db = AppDatabase.getInstance(context)

    @Singleton
    @Provides
    fun provideAppDatabase() = db

    @Singleton
    @Provides
    fun provideDao() = db.launchDao()

    @Singleton
    @Provides
    fun provideLaunchRepository() = LaunchRepository.getInstance(context)

}