package org.fursa.spacex.di

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.fursa.spacex.utils.const.ApiConst
import org.fursa.spacex.api.ApiRequest
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule() {
    private val logger = HttpLoggingInterceptor()
    private var okHttpClient: OkHttpClient
    private var retrofit: Retrofit

    init {
        logger.level = HttpLoggingInterceptor.Level.BODY

        okHttpClient = OkHttpClient.Builder()
            .addInterceptor(logger)
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(ApiConst.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(): ApiRequest {
        return retrofit.create(ApiRequest::class.java)
    }
}