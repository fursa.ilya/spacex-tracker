package org.fursa.spacex.di

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Context.NOTIFICATION_SERVICE
import android.content.SharedPreferences
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import org.fursa.spacex.utils.const.ApiConst
import org.fursa.spacex.background.NotificationHelper
import javax.inject.Singleton

@Module
class ManagerModule(appContext: Context) {

    private val appContext = appContext

    @Singleton
    @Provides
    fun provideAlarmManager() = appContext.getSystemService(ALARM_SERVICE) as AlarmManager

    @Singleton
    @Provides
    fun provideAppContext() = appContext

    @Singleton
    @Provides
    fun provideNotificationManager() = appContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

    @RequiresApi(Build.VERSION_CODES.O)
    @Singleton
    @Provides
    fun provideNotificationChannel() = NotificationChannel(
        ApiConst.CHANNEL_ID,
        ApiConst.CHANNEL_NAME,
        NotificationManager.IMPORTANCE_DEFAULT)

    @Singleton
    @Provides
    fun provideNotificationHelper() = NotificationHelper.getInstance(appContext)

    @Singleton
    @Provides
    fun provideSharedPreferences(): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext)
}