package org.fursa.spacex.di

import dagger.Component
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.MainActivity
import org.fursa.spacex.background.*
import org.fursa.spacex.db.repository.LaunchRepository
import org.fursa.spacex.ui.adapter.PastAdapter
import org.fursa.spacex.ui.adapter.UpcomingAdapter
import org.fursa.spacex.ui.fragments.PastFragment
import org.fursa.spacex.ui.fragments.PastMissionFragment
import org.fursa.spacex.ui.fragments.UpcomingFragment
import org.fursa.spacex.ui.viewmodel.DetailViewModel
import org.fursa.spacex.ui.viewmodel.PastViewModel
import org.fursa.spacex.ui.viewmodel.UpcomingViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [
    CacheModule::class,
    ManagerModule::class,
    MapperModule::class,
    NetworkModule::class])
interface AppComponent {
    fun inject(app: AppDelegate)
    fun inject(repository: LaunchRepository)
    fun inject(viewModel: DetailViewModel)
    fun inject(viewModel: PastViewModel)
    fun inject(viewModel: UpcomingViewModel)
    fun inject(activity: MainActivity)
    fun inject(fragment: PastMissionFragment)
    fun inject(fragment: PastFragment)
    fun inject(fragment: UpcomingFragment)
    fun inject(notificationHelper: NotificationHelper)
    fun inject(worker: AlarmSetterWorker)
    fun inject(receiver: AlarmReceiver)
    fun inject(receiver: BootCompleteReceiver)
    fun inject(pastAdapter: PastAdapter)
    fun inject(upcomingAdapter: UpcomingAdapter)
    fun inject(worker: LaunchInfoUpdateWorker)
}