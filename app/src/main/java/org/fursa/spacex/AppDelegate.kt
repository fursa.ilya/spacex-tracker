package org.fursa.spacex

import android.app.Application
import androidx.work.Configuration
import androidx.work.WorkManager
import org.fursa.spacex.di.*

class AppDelegate : Application() {

    override fun onCreate() {
        super.onCreate()

        val config = Configuration.Builder()
            .build()
        WorkManager.initialize(applicationContext, config)

        injector = DaggerAppComponent
            .builder()
            .cacheModule(CacheModule(applicationContext))
            .managerModule(ManagerModule(applicationContext))
            .networkModule(NetworkModule())
            .build()
        injector.inject(this)
    }

    companion object {
        lateinit var injector: AppComponent
    }
}