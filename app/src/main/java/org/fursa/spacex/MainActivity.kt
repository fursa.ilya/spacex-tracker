package org.fursa.spacex

import android.content.SharedPreferences
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.work.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import org.fursa.spacex.background.AlarmSetterWorker
import org.fursa.spacex.background.LaunchInfoUpdateWorker
import org.fursa.spacex.db.repository.LaunchRepository
import org.fursa.spacex.ui.fragments.PastFragment
import org.fursa.spacex.ui.fragments.UpcomingFragment
import org.fursa.spacex.ui.fragments.menu.MenuFragment
import org.fursa.spacex.ui.tab.ViewPagerAdapter
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private val viewPagerAdapter =
        ViewPagerAdapter(supportFragmentManager, 0)


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        viewPagerAdapter.addFragment(UpcomingFragment.newInstance(), resources.getString(R.string.upcoming))
        viewPagerAdapter.addFragment(PastFragment.newInstance(), resources.getString(R.string.completed))
        view_pager.adapter = viewPagerAdapter

        tab_layout.setupWithViewPager(view_pager)

        tab_layout.getTabAt(0)?.icon = resources.getDrawable(R.drawable.ic_upcoming_primary, theme)
        tab_layout.getTabAt(1)?.icon = resources.getDrawable(R.drawable.ic_completed_primary, theme)

        bottom_app_bar.setNavigationOnClickListener(this)

    }

    override fun onClick(v: View?) {
        val menuFragment = MenuFragment.newInstance()
        menuFragment.show(supportFragmentManager, menuFragment.tag)
    }

}
