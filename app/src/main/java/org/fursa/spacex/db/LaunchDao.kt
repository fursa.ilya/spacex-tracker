package org.fursa.spacex.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

@Dao
interface LaunchDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertNextLaunch(launch: Launch)

    @Query("select * from launch")
    fun getInfo(): Single<Launch>
}