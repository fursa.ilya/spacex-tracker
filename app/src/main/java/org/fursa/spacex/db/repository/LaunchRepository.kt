package org.fursa.spacex.db.repository

import android.content.Context
import org.fursa.spacex.AppDelegate
import org.fursa.spacex.db.AppDatabase
import org.fursa.spacex.db.Launch
import org.fursa.spacex.db.LaunchDao
import javax.inject.Inject

class LaunchRepository(context: Context) {

    init {
        AppDelegate.injector.inject(this)
    }

    @Inject
    lateinit var dao: LaunchDao

    fun upsert(launch: Launch) {
        dao.upsertNextLaunch(launch)
    }

    fun getLaunch() = dao.getInfo()

    companion object {
        fun getInstance(context: Context): LaunchRepository {
            return LaunchRepository(context)
        }
    }
}