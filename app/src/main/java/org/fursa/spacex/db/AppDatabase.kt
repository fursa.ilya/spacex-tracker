package org.fursa.spacex.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import org.fursa.spacex.utils.const.ApiConst.DATABASE_NAME


@Database(entities = [Launch::class], version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun launchDao(): LaunchDao

    companion object {
        fun getInstance(context: Context): AppDatabase {
            val appContext = context.applicationContext
            return Room.databaseBuilder(appContext, AppDatabase::class.java, DATABASE_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}