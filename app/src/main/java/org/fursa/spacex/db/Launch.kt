package org.fursa.spacex.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.fursa.spacex.utils.formatDate

const val CURRENT_LAUNCH_ID = 0

@Entity(tableName = "launch")
data class Launch(
    @ColumnInfo(name = "mission_title")
    val title: String,
    @ColumnInfo(name = "mission_details")
    val details: String,
    @ColumnInfo(name = "mission_timestamp")
    val timestamp: Long = 0,
    @ColumnInfo(name = "flight_number")
    val flightNumber: Int = 0
) {
    @PrimaryKey(autoGenerate = false)
    var id: Int = CURRENT_LAUNCH_ID

    override fun toString(): String {
        return "Launch(" +
                " title='$title', " +
                " details='$details'," +
                " timestamp=${formatDate(timestamp * 1000)}," +
                " flightNumber=$flightNumber," +
                " id=$id)"
    }


}